import React, {Component} from 'react';
import {
  FlatList,
  ImageBackground,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-async-storage/async-storage';

import todayImage from '../../assets/imgs/today.jpg';
import moment from 'moment';
import commonStyles from '../commonStyles';
import Task from '../components/Task';
import AddTask from './AddTask';

const initialState = {
  showDoneTasks: true,
  showAddTask: false,
  visibleTasks: [],
  tasks: [],
};

export default class TaskList extends Component {
  state = {
    ...initialState,
  };

  componentDidMount = async () => {
    const stateString = await AsyncStorage.getItem('tasks-state');
    const state = JSON.parse(stateString) || initialState;

    this.setState(state, this.filterTasks);
  };

  addTask = newTask => {
    const tasks = [...this.state.tasks];
    tasks.push({id: Math.random(), ...newTask, doneAt: null});

    this.setState({tasks, showAddTask: false}, this.filterTasks);
  };

  toogleTask = taskId => {
    const tasks = [...this.state.tasks];
    tasks.forEach(task => {
      if (task.id === taskId) {
        task.doneAt = task.doneAt ? null : Date.now();
      }
    });

    this.setState({tasks}, this.filterTasks);
  };

  toogleFilter = () => {
    this.setState({showDoneTasks: !this.state.showDoneTasks}, this.filterTasks);
  };

  filterTasks = () => {
    let visibleTasks = null;

    if (this.state.showDoneTasks) {
      visibleTasks = [...this.state.tasks];
    } else {
      const isPending = task => task.doneAt === null;
      visibleTasks = this.state.tasks.filter(isPending);
    }

    this.setState({visibleTasks});
    AsyncStorage.setItem('tasks-state', JSON.stringify(this.state));
  };

  deleteTask = id => {
    const tasks = this.state.tasks.filter(task => task.id !== id);

    this.setState({tasks}, this.filterTasks);
  };

  render() {
    const today = moment().locale('en-us').format('dddd, D [of] MMMM');

    return (
      <SafeAreaView style={styles.container}>
        <AddTask isVisible={this.state.showAddTask} onSave={this.addTask} />
        <ImageBackground style={styles.background} source={todayImage}>
          <View style={styles.iconBar}>
            <TouchableOpacity onPress={this.toogleFilter}>
              <Icon
                name={this.state.showDoneTasks ? 'eye' : 'eye-slash'}
                size={20}
                color={commonStyles.colors.secondary}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.titleBar}>
            <Text style={styles.title}>Today</Text>
            <Text style={styles.subtitle}>{today}</Text>
          </View>
        </ImageBackground>
        <View style={styles.taskList}>
          <FlatList
            data={this.state.visibleTasks}
            keyExtractor={item => item.id}
            renderItem={({item}) => (
              <Task
                {...item}
                toogleTask={this.toogleTask}
                onDelete={this.deleteTask}
              />
            )}
          />
        </View>
        <TouchableOpacity
          style={styles.addButton}
          onPress={() => this.setState({showAddTask: true})}
          activeOpacity={0.7}>
          <Icon name="plus" size={20} color={commonStyles.colors.secondary} />
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const textStyle = {
  fontFamily: commonStyles.fontFamily,
  color: commonStyles.colors.secondary,
};

const styles = StyleSheet.create({
  container: {flexGrow: 1},
  background: {flexGrow: 1},
  taskList: {flexGrow: 9},
  titleBar: {flexGrow: 1, justifyContent: 'flex-end'},
  title: {
    ...textStyle,
    fontSize: 50,
    marginLeft: 20,
    marginBottom: 20,
  },
  subtitle: {
    ...textStyle,
    fontSize: 20,
    marginLeft: 20,
    marginBottom: 30,
  },
  iconBar: {
    flexDirection: 'row',
    marginHorizontal: 20,
    justifyContent: 'flex-end',
    marginTop: 10,
  },
  addButton: {
    position: 'absolute',
    right: 30,
    bottom: 30,
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: commonStyles.colors.today,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
