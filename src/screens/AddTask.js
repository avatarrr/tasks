import React, {Component} from 'react';
import {
  Alert,
  Modal,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  ToastAndroid,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';

import DateTimePicker from '@react-native-community/datetimepicker';

import commonStyles from '../commonStyles';
import moment from 'moment';

const initialState = {description: '', date: new Date(), showDatePicker: false};

export default class AddTask extends Component {
  state = {...initialState};

  save = () => {
    const description = this.state.description;

    if (description.trim() === '') {
      const message = "The description can't be empty! 😠";

      Platform.OS === 'android'
        ? ToastAndroid.show(message, ToastAndroid.SHORT)
        : Alert.alert('', message);
      return;
    }

    const newTask = {
      description: this.state.description,
      estimateAt: this.state.date,
    };

    if (this.props.onSave) {
      this.props.onSave(newTask);
      this.setState({...initialState});
    }
  };

  getDatePicker = () => {
    let datePicker = (
      <DateTimePicker
        value={this.state.date}
        onChange={(_, date) => this.setState({date, showDatePicker: false})}
        mode="date"
      />
    );

    const dateString = moment(this.state.date).format('ddd, D of MMMM');

    if (Platform.OS === 'android') {
      datePicker = (
        <View>
          <TouchableOpacity
            onPress={() => this.setState({showDatePicker: true})}>
            <Text style={styles.date}>{dateString}</Text>
          </TouchableOpacity>
          {this.state.showDatePicker && datePicker}
        </View>
      );
    }

    return datePicker;
  };

  render() {
    return (
      <Modal
        transparent
        visible={this.props.isVisible}
        onRequestClose={this.props.onCancel}
        animationType="slide">
        <TouchableWithoutFeedback onPress={this.props.onCancel}>
          <View style={styles.background}>
            <TouchableWithoutFeedback>
              <View style={styles.container}>
                <Text style={styles.header}>New Task</Text>
                <TextInput
                  style={styles.input}
                  placeholder="Description"
                  onChangeText={description => this.setState({description})}
                  value={this.state.description}
                />
                {this.getDatePicker()}
                <View style={styles.buttons}>
                  <TouchableOpacity onPress={this.props.onCancel}>
                    <Text style={styles.button}>Cancel</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={this.save}>
                    <Text style={styles.button}>Save</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    flexGrow: 1,
    backgroundColor: 'rgba(0,0,0,0.7)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    width: '95%',
    backgroundColor: '#FFF',
  },
  header: {
    width: '100%',
    fontFamily: commonStyles.fontFamily,
    backgroundColor: commonStyles.colors.today,
    color: commonStyles.colors.secondary,
    textAlign: 'center',
    padding: 15,
    fontSize: 18,
  },
  buttons: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  button: {
    margin: 20,
    marginRight: 30,
    color: commonStyles.colors.today,
  },
  input: {
    fontFamily: commonStyles.fontFamily,
    height: 40,
    margin: 15,
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#E3E3E3',
    borderRadius: 6,
  },
  date: {
    fontFamily: commonStyles.fontFamily,
    fontSize: 20,
    marginLeft: 15,
  },
});
