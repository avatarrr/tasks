import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import moment from 'moment';

import commonStyles from '../commonStyles';
import {Swipeable} from 'react-native-gesture-handler';

export default props => {
  const doneOrNotStyle = props.doneAt != null ? styles.taskDoneTextStyle : {};

  const date = props.doneAt ? props.doneAt : props.estimateAt;
  const formattedDate = moment
    .utc(date)
    .locale('en-us')
    .format('ddd, D [de] MMMM');

  const onDelete = () => props.onDelete && props.onDelete(props.id);

  const getRightContent = () => {
    return (
      <TouchableOpacity style={styles.right} onPress={onDelete}>
        <Icon name="trash" size={30} color="white" />
      </TouchableOpacity>
    );
  };

  const getLeftContent = () => {
    return (
      <View style={styles.left}>
        <Icon name="trash" size={20} color="white" style={styles.removeIcon} />
        <Text style={styles.removeText}>Remove</Text>
      </View>
    );
  };

  return (
    <Swipeable
      renderRightActions={getRightContent}
      renderLeftActions={getLeftContent}
      onSwipeableLeftOpen={onDelete}>
      <View style={styles.container}>
        <TouchableWithoutFeedback onPress={() => props.toogleTask(props.id)}>
          <View style={styles.checkContainer}>
            {getCheckView(props.doneAt)}
          </View>
        </TouchableWithoutFeedback>
        <View>
          <Text style={[styles.description, doneOrNotStyle]}>
            {props.description}
          </Text>
          <Text style={styles.date}>{formattedDate}</Text>
        </View>
      </View>
    </Swipeable>
  );
};

function getCheckView(doneAt) {
  return doneAt ? (
    <View style={styles.done}>
      <Icon name="check" size={20} color="white" />
    </View>
  ) : (
    <View style={styles.pending} />
  );
}

const checkViewCommonStyle = {height: 25, width: 25, borderRadius: 13};
const textStyleCommon = {fontFamily: commonStyles.fontFamily};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderColor: '#AAA',
    borderBottomWidth: 1,
    alignItems: 'center',
    paddingVertical: 10,
    backgroundColor: 'white',
  },
  checkContainer: {width: '20%', alignItems: 'center'},
  pending: {
    ...checkViewCommonStyle,
    borderWidth: 1,
    borderColor: '#555',
  },
  done: {
    ...checkViewCommonStyle,
    backgroundColor: '#4D7031',
    justifyContent: 'center',
    alignItems: 'center',
  },
  description: {
    ...textStyleCommon,
    color: commonStyles.colors.mainText,
    fontSize: 15,
  },
  taskDoneTextStyle: {textDecorationLine: 'line-through'},
  date: {
    ...textStyleCommon,
    color: commonStyles.colors.subText,
    fontSize: 12,
  },
  right: {
    backgroundColor: 'red',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
  },
  left: {
    backgroundColor: 'red',
    flexDirection: 'row',
    alignItems: 'center',
    flexGrow: 1,
  },
  removeText: {
    fontFamily: commonStyles.fontFamily,
    color: 'white',
    fontSize: 20,
    margin: 10,
  },
  removeIcon: {
    marginLeft: 10,
  },
});
